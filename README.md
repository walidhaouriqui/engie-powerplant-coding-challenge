# Engie Powerplant Coding Challenge - README

Welcome to the Engie Powerplant Coding Challenge! This project is built using .NET 7 and provides an API for managing a powerplant's production plan. This README will guide you through the setup process and provide information on how to use the API.

## Setup

To get started, please follow the steps below:

### Prerequisites

- [.NET 7 SDK](https://dotnet.microsoft.com/download/dotnet/7.0)
- [Docker](https://www.docker.com/get-started) (optional)

### 1. Clone the Repository

Begin by cloning the repository to your local machine:

git clone <https://gitlab.com/walidhaouriqui/engie-powerplant-coding-challenge.git>
cd engie-powerplant-coding-challenge
Build the Project

#### Option 2: With Docker

If you prefer to run the application using Docker, you can build and run a Docker container. 

Build the Docker image:

docker build -t <container_name> .

Run the Docker container:
docker run -p 8888:8888 <container_name>

Note: Replace `<container_name>` with a name of your choice.

The Engie Powerplant Coding Challenge API provides endpoints for managing the production plan. You can interact with the API using tools like Postman or Swagger.

To access the API, use the following base URL:
via swagger: http://0.0.0.0:8888/swagger/index.html 
via postman: http://0.0.0.0:8888/ProductionPlan


## Conclusion

You have successfully set up and started the Engie Powerplant Coding Challenge API. Now you can explore the endpoints and use the provided tools to interact with the API.