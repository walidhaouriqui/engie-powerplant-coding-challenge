﻿using System;
using System.ComponentModel.DataAnnotations;

namespace engie_powerplant_coding_challenge
{
    public class ProductionPlanPayload
    {
        [Required]
        public int Load { get; set; }

        public Fuels? Fuels { get; set; }

        public List<PowerPlant>? PowerPlants { get; set; }
    }


    public class PowerPlantResponse
    {
        public string N { get; set; } = string.Empty;
        public double P { get; set; }
    }

    public class ProductionPlanResponse
    {
        public List<PowerPlantResponse> PowerPlants { get; set; } = new List<PowerPlantResponse>();
    }
}

