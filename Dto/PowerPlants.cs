﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace engie_powerplant_coding_challenge
{

    public interface IPowerPlant
    {
        string Name { get; }
        string Type { get; }
        double Efficiency { get; }
        int Pmin { get; }
        int Pmax { get; }
    }

    public class PowerPlant: IPowerPlant
    {
        [Required]
        public string Name { get; set; } = string.Empty;

        [Required]
        public string Type { get; set; } = string.Empty;

        [Required]
        public double Efficiency { get; set; }

        [Required]
        public int Pmin { get; set; }

        [Required]
        public int Pmax { get; set; }
    }

    public class Fuels
    {
        [JsonPropertyName("gas(euro/MWh)")]
        public double? Gas { get; set; }

        [JsonPropertyName("kerosine(euro/MWh)")]
        public double? Kerosine { get; set; }

        [JsonPropertyName("co2(euro/ton)")]
        public double? CO2 { get; set; }

        [JsonPropertyName("wind(%)")]
        public double? Wind { get; set; }

        // find out right fuel price based on powerplant type
        public double GetFuelPriceOf(PowerPlant powerPlant)
        {
            double price;

            // find out base price of fuel type
            switch (powerPlant.Type)
            {
                case "gasfired":
                    price = Gas ?? 0;
                    break;
                case "turbojet":
                    price = Kerosine ?? 0;
                    break;
                case "windturbine":
                    return 0; // Wind-turbines do not consume 'fuel' and thus are considered to generate power at zero price.
                default:
                    throw new Exception("unknown type " + powerPlant.Type);
            }

            // take into concideration efficiency to adapt price
            var res = price / powerPlant.Efficiency;

            // add CO2 pricing on gasfired fuel
            if (powerPlant.Type == "gasfired")
            {
                var generated_mwh = 1 / powerPlant.Efficiency; // for instance if 50%, we need to generate 2mwh, to get finaly 1 (CO2 based on 2mwh)
                res = res + (generated_mwh * 0.3 * (CO2 ?? 0));
            }

            return res;
        }


        // get generated power depending on pmin, pmax and efficiency
        public double ComputeGeneratedPower(PowerPlant powerPlant)
        {
            double e = powerPlant.Efficiency;
            if (powerPlant.Type == "windturbine")
            {
                e = (Wind ?? 0) / 100; // use average wind as efficiency
            }

            var res = Math.Max(powerPlant.Pmin, e * powerPlant.Pmax);
            return Math.Round(res * 10) / 10; // round to 0.1 precision
        }
    }

}



