﻿using System;
namespace engie_powerplant_coding_challenge.Services
{
	public interface IProductPlanService
    {
        List<PowerPlantResponse> GenerateProductionPlan(ProductionPlanPayload payload);
        List<PowerPlantResponse> FindCombination(Fuels fuels, List<PowerPlant> powerPlants, double load);

    }
}

