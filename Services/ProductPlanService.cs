﻿using System;
namespace engie_powerplant_coding_challenge.Services
{
	public class ProductPlanService : IProductPlanService
    {
        public List<PowerPlantResponse> GenerateProductionPlan(ProductionPlanPayload payload)
        {
            var fuels = payload.Fuels ?? new Fuels();
            var powerPlants = payload.PowerPlants ?? new List<PowerPlant>();

            // sort from cheapest to most expensive
            powerPlants.Sort((p1, p2) => fuels.GetFuelPriceOf(p1).CompareTo(fuels.GetFuelPriceOf(p2)));

            // find exact match with requested load
            var res = FindCombination(fuels, powerPlants, payload.Load);

            return res;
        }

        public List<PowerPlantResponse> FindCombination(Fuels fuels, List<PowerPlant> powerPlants, double load)
        {
            var combination = new List<PowerPlantResponse>();
            var remaining = load;

            foreach (var item in powerPlants)
            {
                if (remaining < item.Pmin)
                {
                    continue;
                }

                var power = fuels.ComputeGeneratedPower(item);
                if (power == 0)
                {
                    continue;
                }

                if (power > remaining)
                {
                    power = remaining;
                }

                combination.Add(new PowerPlantResponse { N = item.Name, P = power });

                remaining = Math.Round((remaining - power) * 10) / 10;
                if (remaining <= 0)
                {
                    break;
                }
            }

            return combination;
        }

    }
}


