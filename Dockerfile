FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /app
COPY . .
RUN dotnet restore ./engie-powerplant-coding-challenge.csproj && \
   dotnet build --configuration Release --no-restore ./engie-powerplant-coding-challenge.csproj && \
   dotnet publish --configuration Release --no-build --output ./publish ./engie-powerplant-coding-challenge.csproj


FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS runtime
WORKDIR /app
EXPOSE 8888
ENV ASPNETCORE_URLS=http://+:8888/
COPY --from=build /app/publish .

ENTRYPOINT ["dotnet", "engie-powerplant-coding-challenge.dll"]