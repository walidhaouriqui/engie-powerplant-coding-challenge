﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net;
using engie_powerplant_coding_challenge.Services;

namespace engie_powerplant_coding_challenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductionPlanController : ControllerBase
    {
        private readonly ILogger<ProductionPlanController> _logger;
        private readonly IProductPlanService _productPlanService;

        public ProductionPlanController(ILogger<ProductionPlanController> logger, IProductPlanService productPlanService)
        {
            _logger = logger;
            _productPlanService = productPlanService; 
        }

        [HttpPost]
        public IActionResult Post([Required] ProductionPlanPayload? payload)
        {
            if (payload is null)
            {
                return BadRequest("Invalid payload");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = _productPlanService.GenerateProductionPlan(payload);
            return Ok(response);
        }


    }
}



